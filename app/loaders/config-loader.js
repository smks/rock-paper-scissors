let configuration = {};

const loadConfig = (images = []) => {
  return new Promise((resolve, reject) => {
    fetch('game.json')
    .then(response => response.json())
    .then((config) => {
      configuration = config;
      resolve();
    });
  });
};

export const getConfig = () => { 
  return configuration;
}

export default loadConfig;