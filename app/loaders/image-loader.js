import { getConfig } from './config-loader';

let imageElements = {};

const loadImage = (imageSource, id) => {
  return new Promise((resolve, reject) => {
    var img = new Image();
    img.id = id;
    img.className = 'choice';
    img.src = imageSource;
    img.dataset.choice = id;
    img.onload = () => {
      imageElements[id] = img;
      resolve();
    };
  });
}

const loadImages = () => {

  const { theme, images } = getConfig();
  const { path } = theme;

  if (images.length === 0) {
    console.warn('No images passed in');
    return;
  }

  let imagesToLoad = [];

  Object.keys(images).map((key, index) => {
    const img = `${path}/${images[key]}`;
    imagesToLoad.push(loadImage(img, key));
  })

  return Promise.all(imagesToLoad);
};

const getImageElements = () => {
  return imageElements;
};

export { loadImages, getImageElements };