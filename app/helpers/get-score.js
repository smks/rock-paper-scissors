import { PLAYER, CPU, DRAW } from './../constants/decisions';

export default function getScore(whoWon) {
  const { localStorage } = window;
  const currentScorePlayer = parseInt(localStorage.getItem(PLAYER), 10) || 0;
  const currentScoreCPU = parseInt(localStorage.getItem(CPU), 10) || 0;
  
  return {
    player: currentScorePlayer,
    cpu: currentScoreCPU
  }
};